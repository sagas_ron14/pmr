<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Home';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>PRM</h1>
        <p class="lead">Patient Records Management</p>
        <div class="row col-md-offset-3" id="pmr-records-options">
            <?=Html::a('<i class="glyphicon glyphicon-folder-open"></i> Records', ['/records'], ['class'=>'btn btn-lg btn-success col-md-4'])?>
            <?=Html::a('<i class="glyphicon glyphicon-off"></i> API', ['/api'], ['class'=>'btn btn-lg btn-default col-md-4'])?>
        </div>
    </div>

</div>

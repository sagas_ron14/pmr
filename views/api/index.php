<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'API';
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?=Yii::$app->name?> API Guidelines</h3>
<hr>
<div>
    <div>
        The api supports the following methods
        <table class="table table-responsive">
            <thead>
                <tr><th>Method</th><th>Root URL</th><th>Description</th></tr>
            </thead>
            <tbody>
                <tr><td>district</td><td><?=Url::to(['/api/district'], true)?></td><td>Provides access to information about districts</td></tr>
                <tr><td>region</td><td><?=Url::to(['/api/region'], true)?></td><td>Provides access to information about regions</td></tr>
                <tr><td>patient</td><td><?=Url::to(['/api/patient'], true)?></td><td>Provides access to information about patients</td></tr>
                <tr><td>facility</td><td><?=Url::to(['/api/facility'], true)?></td><td>Provides access to information about facilities</td></tr>
                <tr><td>facility-group</td><td><?=Url::to(['/api/facility-group'], true)?></td><td>Provides access to information about facility groups</td></tr>
            </tbody>
        </table>

        <h4>Parameters</h4>
        <table class="table table-responsive">
            <thead>
                <tr><th>Parameter</th><th>Required</th><th>Description</th></tr>
            </thead>
            <tbody>
                <tr>
                    <td>id</td><td>NO</td>
                    <td>
                        Returns data corresponding to the object corresponding to that id. An empty object is returned if the object cannot be found.
                        If this parameter is not supplied, all known elements will be returned
                    </td>
                </tr>
                <tr>
                    <td>format</td><td>NO</td>
                    <td>
                        Returns the data in the format specified. supported formats include <strong>json</strong>, <strong>xml</strong>.
                        If this parameter is not supplied, the <strong>json</strong> format will be used by default.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>



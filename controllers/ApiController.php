<?php

namespace app\controllers;


use app\models\Inflect;
use yii\filters\auth\HttpBasicAuth;
use yii\web\Response;

class ApiController extends \yii\web\Controller
{
    public function init() {
        parent::init();
        \Yii::$app->errorHandler->errorAction='/api/error';
    }

    public function behaviors()
    {
        $user_agent = $this->getUserAgent();

        if($user_agent == 'curl'){
            \Yii::$app->user->enableSession = false;
            \Yii::$app->user->enableAutoLogin = false;
            $behaviors = parent::behaviors();
            $behaviors['authenticator'] = [
                'class' => HttpBasicAuth::className(),
                'except' => ['index','test'],
            ];

            return $behaviors;
        }

        if(\Yii::$app->user->isGuest)
            \Yii::$app->user->loginRequired();

        return [];

    }

    public function actionDistrict($id = null, $format = null)
    {
        return $this->fetchData('app\models\District', $id, $format);
    }

    public function actionFacility($id = null, $format = null)
    {
        return $this->fetchData('app\models\Facility', $id, $format);
    }

    public function actionFacilityGroup($id = null, $format = null)
    {
        return $this->fetchData('app\models\FacilityGroup', $id, $format);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPatient($id = null, $format = null)
    {
        return $this->fetchData('app\models\Patient', $id, $format);
    }

    public function actionRegion($id = null, $format = null)
    {
        return $this->fetchData('app\models\Region', $id, $format);
    }

    private function validateId($id)
    {
        return $id && is_numeric($id);
    }

    private function fetchData($class_name, $id, $format){
        $result = [];
        if($this->validateId($id)){
            if(($object = $class_name::findOne($id)) != null)
                $result = $object->asArray();
        }else{
            $objects = $class_name::getAllAsArray();
            if(count($objects))
                $result[Inflect::pluralize(end(explode("\\", $class_name)))] = $objects;
        }

        return $this->formatResponse($format, $result);
    }

    private function formatResponse($format, $data)
    {
        switch($format){
            case 'xml':
                \Yii::$app->response->format = Response::FORMAT_XML;
                break;
            default:
                \Yii::$app->response->format = Response::FORMAT_JSON;
        }

        return $data;
    }

    private function getUserAgent(){
        $user_agent_string = \Yii::$app->request->headers['user-agent'];
        $user_agent_parts = explode('/', $user_agent_string);
        $user_agent = '';
        if(count($user_agent_parts))
            $user_agent = strtolower($user_agent_parts[0]);

        return $user_agent;
    }

    public function actionError()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var \yii\base\Exception $exception */

        if($exception = \Yii::$app->errorHandler->exception)
        {
            return ['error'=>[
                'code'=>$exception->getCode(),
                'name'=>$exception->getName(),
                'message'=>$exception->getMessage()]
            ];
        }

        \Yii::$app->end();
    }



}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_162322_create_region_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('region', [
            'id'=>'int primary key auto_increment',
            'name'=>'text',
            'description'=>'text',
            'created_on'=>'text',
            'created_by'=>'int',
            'updated_on'=>'text',
            'updated_by'=>'int',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('region');
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_163512_create_facility_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('facility', [
            'id'=>'int primary key auto_increment',
            'name'=>'text',
            'description'=>'text',
            'code'=>'text',
            'facility_group_id'=>'int',
            'location_id'=>'int',
            'region_id'=>'int',
            'created_on'=>'text',
            'created_by'=>'int',
            'updated_on'=>'text',
            'updated_by'=>'int',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('facility');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_164101_patient_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('patient', [
            'id'=>'int primary key auto_increment',
            'first_name'=>'text',
            'last_name'=>'text',
            'other_names'=>'text',
            'date_of_birth'=>'text',
            'contact_no'=>'text',
            'next_of_kin_name'=>'text',
            'next_of_kin_contact_no'=>'text',
            'facility_id'=>'int',
            'created_on'=>'text',
            'created_by'=>'int',
            'updated_on'=>'text',
            'updated_by'=>'int',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('patient');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

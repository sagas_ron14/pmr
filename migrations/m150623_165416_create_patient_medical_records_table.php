<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_165416_create_patient_medical_records_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('patient_medical_records', [
            'id'=>'int primary key auto_increment',
            'patient_id'=>'int',
            'facility_id'=>'int',
            'observation'=>'text',
            'diagnosis'=>'text',
            'treatment'=>'text',
            'date_of_next_visit'=>'text',
            'created_on'=>'text',
            'created_by'=>'int',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('patient_medical_records');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

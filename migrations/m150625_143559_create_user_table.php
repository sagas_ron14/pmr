<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_143559_create_user_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('user', array(
            'id'=>'int primary key auto_increment',
            'username'=>'varchar(30)',
            'password'=>'text',
            'name'=>'text',
            'email'=>'text',
            'role'=>'text',
            'id_number'=>'text',
            'status'=>'varchar(50)',
            'access_token'=>'text',
            'access_token_type'=>'text',
            'token_expiry'=>'datetime',
            'is_logged_in'=>'varchar(2)',
            'last_activity_time'=>'datetime',
            'last_login_time'=>'datetime',
            'created_on'=>'timestamp not null default current_timestamp',
        ));
    }

    public function safeDown()
    {
        $this->dropTable('user');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

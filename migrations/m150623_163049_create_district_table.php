<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_163049_create_district_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('district', [
            'id'=>'int primary key auto_increment',
            'name'=>'text',
            'description'=>'text',
            'created_on'=>'text',
            'created_by'=>'int',
            'updated_on'=>'text',
            'updated_by'=>'int',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('district');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}

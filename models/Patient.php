<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $other_names
 * @property string $date_of_birth
 * @property string $contact_no
 * @property string $next_of_kin_name
 * @property string $next_of_kin_contact_no
 * @property integer $facility_id
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 *
 * @property Facility $facility
 */
class Patient extends PMRActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'other_names', 'date_of_birth', 'contact_no', 'next_of_kin_name', 'next_of_kin_contact_no', 'created_on', 'updated_on'], 'string'],
            [['facility_id', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'other_names' => 'Other Names',
            'date_of_birth' => 'Date Of Birth',
            'contact_no' => 'Contact No',
            'next_of_kin_name' => 'Next Of Kin Name',
            'next_of_kin_contact_no' => 'Next Of Kin Contact No',
            'facility_id' => 'Facility',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getFacility()
    {
        return $this->hasOne(Facility::className(), ['id' => 'facility_id']);
    }

    public function asArray($withRelations = true){
        $this->omitFields[] = 'facility_id';
        return parent::asArray();
    }

    protected $relations = ['facility'];
}

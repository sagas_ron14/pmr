<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patient_medical_records".
 *
 * @property integer $id
 * @property integer $patient_id
 * @property integer $facility_id
 * @property string $observation
 * @property string $diagnosis
 * @property string $treatment
 * @property string $date_of_next_visit
 * @property string $created_on
 * @property integer $created_by
 */
class MedicalRecord extends PMRActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_medical_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id', 'facility_id', 'created_by'], 'integer'],
            [['observation', 'diagnosis', 'treatment', 'date_of_next_visit', 'created_on'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'facility_id' => 'Facility ID',
            'observation' => 'Observation',
            'diagnosis' => 'Diagnosis',
            'treatment' => 'Treatment',
            'date_of_next_visit' => 'Date Of Next Visit',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "facility".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property integer $facility_group_id
 * @property integer $location_id
 * @property integer $region_id
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 *
 *
 * @property FacilityGroup $facilityGroup
 * @property District $district
 * @property Region $region
 */
class Facility  extends PMRActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'code', 'created_on', 'updated_on'], 'string'],
            [['facility_group_id', 'location_id', 'region_id', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'code' => 'Code',
            'facility_group_id' => 'Facility Group',
            'location_id' => 'District',
            'region_id' => 'Region',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getFacilityGroup()
    {
        return $this->hasOne(FacilityGroup::className(), ['id' => 'facility_group_id']);
    }

    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'location_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Patient::updateAll(['facility_id'=>null], ['facility_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }


    public function asArray($withRelations = true){
        $this->omitFields = ArrayHelper::merge($this->omitFields, ['location_id', 'region_id', 'facility_group_id']);
        return parent::asArray($withRelations);
    }

    protected $relations = ['facilityGroup', 'district', 'region'];
}

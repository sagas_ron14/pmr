<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "facility_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 *
 * @property Facility[] $facilities
 */
class FacilityGroup extends PMRActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facility_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'created_on', 'updated_on'], 'string'],
            [['created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Facility::updateAll(['facility_group_id'=>null], ['facility_group_id'=>$this->id]);
            return true;
        } else {
            return false;
        }
    }

    public function asArray($withRelations = true){
        return parent::asArray($withRelations);
    }

    protected $relations = [];
    protected $child_relations = ['facilities'];

    public function getFacilities()
    {
        return $this->hasMany(Facility::className(), ['facility_group_id'=>'id']);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/25/15
 * Time: 11:22 AM *
 *
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 */

namespace app\models;

use yii\db\Expression;

class PMRActiveRecord extends \yii\db\ActiveRecord {

    protected $omitFields = ['created_by', 'updated_by'];

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert)
                $this->created_on = new Expression('CURRENT_TIMESTAMP');
            else
                $this->updated_on = new Expression('CURRENT_TIMESTAMP');
            return true;
        } else {
            return false;
        }
    }

    public static function getAllAsArray(){
        $result = [];

        $records = self::find()->all();

        foreach($records as $record)
            $result[] = $record->asArray();

        return $result;
    }

    public function asArray($withRelations = true){
        $result = [];

        foreach($this->attributes as $property => $value)
            if(!in_array($property, $this->omitFields))
                $result[str_replace(' ', '', ucwords(str_replace('_',' ', $property)))] = $value;


        if($withRelations && !empty($this->relations))
            foreach($this->relations as $relation)
                $result[ucfirst($relation)] = isset($this->{$relation})
                    ?$this->{$relation}->asArray(false)
                    :[];

        if($withRelations && isset($this->child_relations) && !empty($this->child_relations))
            foreach($this->child_relations as $child_relation){
                $result[ucfirst($child_relation)] = [];
                if(!empty($this->{$child_relation})){
                    foreach($this->{$child_relation} as $relationObject){
                        $result[ucfirst($child_relation)][] = $relationObject->asArray(false);
                    }
                }
            }

        return $result;
    }


}
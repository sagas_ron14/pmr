<?php

namespace app\modules\user\models;

class UserIdentity extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $role;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = User::findOne($id);
        return self::createStatic($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user =  User::find()
            ->where('username=:token',
                ['token'=>$token])->one();
        return self::createStatic($user);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = User::find()->where(['username'=>$username])->one();
        return self::createStatic($user);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return User::validateUserPassword($password, $this->password);
    }

    /**
     * @param $user User
     * @return UserIdentity|null
     */
    private static function createStatic($user)
    {
        if($user == null)
            return null;

        $identity = new self;
        $identity->id = $user->id;
        $identity->username = $user->username;
        $identity->password = $user->password;
        $identity->authKey = $user->access_token_type;
        $identity->accessToken = $user->access_token;
        $identity->role = $user->role;

        return $identity;
    }

    public function logLogin(){
        \Yii::$app->session->set('role', $this->role);
        return User::updateAll(['is_logged_in'=>1], ['id'=>$this->id]);
    }

    public static function logLogout(){
        return User::updateAll(['is_logged_in'=>null], ['id'=>\Yii::$app->user->id]);
    }

}

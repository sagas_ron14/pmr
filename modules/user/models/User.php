<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $role
 * @property string $id_number
 * @property string $status
 * @property string $access_token
 * @property string $access_token_type
 * @property string $token_expiry
 * @property string $is_logged_in
 * @property string $last_activity_time
 * @property string $last_login_time
 * @property string $created_on
 */
class User extends \yii\db\ActiveRecord
{
    const SCENARIO_CHANGE_AUTH = 'change-auth';
    const ROLE_ADMIN = 'admin';
    public static $roles = ['user', 'admin', 'api'];
    public static $status = ['active', 'inactive'];

    const USERNAME_PATTERN = '/^[.-_a-z0-9A-Z]{5,20}$/';
    const PASSWORD_PATTERN = '#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#';

    public $changePassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'name', 'email', 'role', 'id_number', 'access_token', 'access_token_type'], 'string'],
            [['token_expiry', 'last_activity_time', 'last_login_time', 'created_on'], 'safe'],
            [['username'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 50],
            [['is_logged_in'], 'string', 'max' => 2],
            [['role'], 'in', 'range'=>self::$roles, 'strict'=>true],

            [['email'], 'email'],
            [['username'], 'usernameIsUnique'],
            [['status'], 'default', 'value'=>self::$status[0],],
            [['status'], 'in', 'range'=>self::$status, 'strict'=>true],
            [['username'], 'match', 'pattern'=> self::USERNAME_PATTERN,
                'message' => 'Your username can only contain 5 to 20 alphanumeric and/or underscore characters'],
            [['password'], 'match', 'pattern'=> self::PASSWORD_PATTERN,
                'message' => 'Password must be at least 8 characters long with at least one uppercase letter and a digit.'],
        ];
    }

    public function usernameIsUnique(){
        if ($this->isNewRecord && (null != $this->username) && (null != self::find()->where(['username'=>$this->username])->one())){
            $this->addError('username', 'This username is already taken');
            return false;
        }

        if (!$this->isNewRecord && (null != self::find()->where("username = '{$this->username}' and id !='{$this->id}'")->one())){
            $this->addError('username', 'This username is already taken');
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'name' => 'Name',
            'email' => 'Email',
            'role' => 'Role',
            'id_number' => 'ID Number',
            'status' => 'Status',
            'access_token' => 'Access Token',
            'access_token_type' => 'Access Token Type',
            'token_expiry' => 'Token Expiry',
            'is_logged_in' => 'Is Logged In',
            'last_activity_time' => 'Last Activity Time',
            'last_login_time' => 'Last Login Time',
            'created_on' => 'Created On',
        ];
    }

    public function hashPassword($password = null){
        $passwordEmpty = [null, ''];

        $this->password = (!in_array($password, $passwordEmpty))
            ?$password:$this->password;

        $this->password = (!in_array($this->password, $passwordEmpty))
            ?Yii::$app->getSecurity()->generatePasswordHash($this->password):null;
    }

    public function validatePassword($password){
        return self::validateUserPassword($password, $this->password);
    }

    public static function validateUserPassword($password, $hash){
        return Yii::$app->getSecurity()->validatePassword($password, $hash);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert)
                $this->hashPassword();
            else{
                if($this->changePassword)
                    $this->hashPassword();
            }
            return true;
        } else {
            return false;
        }
    }

}

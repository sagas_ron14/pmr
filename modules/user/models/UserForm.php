<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/24/15
 * Time: 12:05 PM
 */

namespace app\modules\user\models;


class UserForm {

    public static function getRoles(){
        return self::buildList(User::$roles);
    }

    public static function getStatuses(){
        return self::buildList(User::$status);
    }

    private static function buildList($items = []){

        $list = [];
        if($items && !empty($items))
            foreach($items as $item)
                $list[$item] = ucwords(implode(' ', preg_split("/[\s,-_.,]+/", strtolower($item))));

        return $list;
    }
}
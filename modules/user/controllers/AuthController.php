<?php

namespace app\modules\user\controllers;

use app\modules\user\models\LoginForm;
use app\modules\user\models\User;
use app\modules\user\models\UserIdentity;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AuthController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        UserIdentity::logLogout();
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpdate()
    {
        $model = User::findOne(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/) {
            $model->changePassword = ($_POST['User']['changePassword'])?true:false;
            if($model->save())
                return $this->redirect(Yii::$app->user->returnUrl);
        }

        return $this->render('@app/modules/user/views/admin/update-auth', [
            'model' => $model,
        ]);
    }

}

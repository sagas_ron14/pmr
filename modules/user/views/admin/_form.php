<?php

use app\modules\user\models\User;
use app\modules\user\models\UserForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$scenario_change_auth = ($model->scenario == User::SCENARIO_CHANGE_AUTH);

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($model->isNewRecord || $scenario_change_auth):?>
        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput($scenario_change_auth ? ['disabled'=>'disabled', 'placeholder'=>'(Leave Unchanged)']:[])?>
    <?php endif?>
    
    <?php if($scenario_change_auth):?>
        <?= $form->field($model, 'changePassword')->checkbox()?>
        <script type="application/javascript">
            document.getElementById('user-changepassword').addEventListener('click', function(){
                var passwordField = document.getElementById('user-password');
                if(this.checked)
                    passwordField.disabled = false;
                else{
                    passwordField.value = '';
                    passwordField.disabled = true;
                }
            });
        </script>
    <?php endif?>
    
    
    <?php if(!$scenario_change_auth):?>
        <?= $form->field($model, 'name')?>
        
        <?= $form->field($model, 'email')?>
        
        <?= $form->field($model, 'role')->dropDownList(UserForm::getRoles())?>
        
        <?= $form->field($model, 'id_number')?>
        
        <?= $form->field($model, 'status')->dropDownList(UserForm::getStatuses()) ?>
    <?php endif?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

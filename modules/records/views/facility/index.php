<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FacilitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $facility_groups [] */
/* @var $regions [] */
/* @var $districts [] */

$this->title = 'Facilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facility-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Facility', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name:ntext',
            //'description:ntext',
            'code:ntext',
            ['label'=>$searchModel->getAttributeLabel('facility_group_id'), 'value'=>'facilityGroup.name',
                'filter'=>Html::activeDropDownList($searchModel, 'facility_group_id', $facility_groups, ['class'=>'form-control','prompt' => 'Select Facility Groups'])],
            ['label'=>$searchModel->getAttributeLabel('location_id'), 'value'=>'district.name',
                'filter'=>Html::activeDropDownList($searchModel, 'location_id', $districts, ['class'=>'form-control','prompt' => 'Select District'])],
            ['label'=>$searchModel->getAttributeLabel('region_id'), 'value'=>'region.name',
                'filter'=>Html::activeDropDownList($searchModel, 'region_id', $regions, ['class'=>'form-control','prompt' => 'Select Region'])],
            // 'created_on:ntext',
            // 'created_by',
            // 'updated_on:ntext',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

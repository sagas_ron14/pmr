<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Facility */
/* @var $facility_groups [] */
/* @var $regions [] */
/* @var $districts [] */

$this->title = 'Update Facility: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facility-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'facility_groups' => $facility_groups,
        'districts' => $districts,
        'regions' => $regions,
    ]) ?>

</div>

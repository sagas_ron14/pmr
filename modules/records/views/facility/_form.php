<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Facility */
/* @var $form yii\widgets\ActiveForm */

/* @var $facility_groups [] */
/* @var $regions [] */
/* @var $districts [] */
?>

<div class="facility-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'facility_group_id')->dropDownList(ArrayHelper::merge([''=>'Select Facility Group'], $facility_groups)) ?>

    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::merge([''=>'Select District'], $districts)) ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::merge([''=>'Select Region'], $regions)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Facility */
/* @var $facility_groups [] */
/* @var $regions [] */
/* @var $districts [] */

$this->title = 'Create Facility';
$this->params['breadcrumbs'][] = ['label' => 'Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facility-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'facility_groups' => $facility_groups,
        'districts' => $districts,
        'regions' => $regions,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FacilityGroup */

$this->title = 'Create Facility Group';
$this->params['breadcrumbs'][] = ['label' => 'Facility Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facility-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

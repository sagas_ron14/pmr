<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patient */

$this->title = $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'first_name:ntext',
            'last_name:ntext',
            'other_names:ntext',
            'date_of_birth:ntext',
            'contact_no:ntext',
            'next_of_kin_name:ntext',
            'next_of_kin_contact_no:ntext',
            ['label'=>'Facility', 'attribute'=>'facility.name'],
//            'created_on:ntext',
//            'created_by',
//            'updated_on:ntext',
//            'updated_by',
        ],
    ]) ?>

</div>

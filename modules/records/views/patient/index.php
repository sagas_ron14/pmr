<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $facilities [] */

$this->title = 'Patients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Patient', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'first_name:ntext',
            'last_name:ntext',
            'other_names:ntext',
            //'date_of_birth:ntext',
            'contact_no:ntext',
            'next_of_kin_name:ntext',
            // 'next_of_kin_contact_no:ntext',
            ['label'=>$searchModel->getAttributeLabel('facility_id'), 'value'=>'facility.name',
                'filter'=>Html::activeDropDownList($searchModel, 'facility_id', $facilities, ['class'=>'form-control','prompt' => 'Select Facility'])],
            // 'created_on:ntext',
            // 'created_by',
            // 'updated_on:ntext',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

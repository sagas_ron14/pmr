<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Patient */
/* @var $form yii\widgets\ActiveForm */
/* @var $facilities [] */
?>

<div class="patient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')?>

    <?= $form->field($model, 'last_name')?>

    <?= $form->field($model, 'other_names')?>

    <?= $form->field($model, 'date_of_birth')->input('date')?>

    <?= $form->field($model, 'contact_no') ?>

    <?= $form->field($model, 'next_of_kin_name') ?>

    <?= $form->field($model, 'next_of_kin_contact_no') ?>

    <?= $form->field($model, 'facility_id')->dropDownList(ArrayHelper::merge([''=>'Select Facility'], $facilities)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Html;

$this->title = 'Records Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $pages = array(
    array('text'=>'<i class="glyphicon glyphicon-user"></i> Patients', 'class'=>'warning', 'link'=>['/records/patient']),
    array('text'=>'<i class="glyphicon glyphicon-home"></i> Facilities', 'class'=>'danger', 'link'=>['/records/facility']),
    array('text'=>'<i class="glyphicon glyphicon-folder-open"></i> Facility Groups', 'class'=>'default', 'link'=>['/records/facility-group']),
    array('text'=>'<i class="glyphicon glyphicon-map-marker"></i> Districts','class'=>'info', 'link'=>['/records/district']),
    array('text'=>'<i class="glyphicon glyphicon-globe"></i> Regions','class'=>'primary', 'link'=>['/records/region']),
)?>
<?php $page_count = count($pages)?>

    
<div class="records-default-index">
    <div class="jumbotron">
        <h2>Patient Records</h2>
        <p class="lead">You are able to manage patient records from here</p>
        <div class="row" id="pmr-records-options">
            <?php if($page_count){
                foreach($pages as $page):?>
                    <?=Html::a($page['text'], $page['link'], ['class'=>'btn btn-lg btn-'.$page['class']])?>
            <?php endforeach; }?>
        </div>
    </div>
</div>

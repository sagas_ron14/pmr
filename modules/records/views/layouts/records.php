<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/23/15
 * Time: 8:33 PM
 * @var string $content
 */
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<?php $this->beginContent('@app/views/layouts/main.php',[]); ?>
    <div class="records">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar" id="pmr-sidebar">
                    <span class="lead col-sm-offset-1"><?=Html::a('Records', ['/records'])?></span>
                    <?=Nav::widget([
                        'options' => ['class' => 'nav nav-sidebar'],
                        'items' => [
                            ['label' => 'Patients', 'url' => ['/records/patient'], 'active'=>(Yii::$app->controller->id == 'patient')],
                            ['label' => 'Facilities', 'url' => ['/records/facility'], 'active'=>(Yii::$app->controller->id == 'facility')],
                            ['label' => 'Facility Groups', 'url' => ['/records/facility-group'], 'active'=>(Yii::$app->controller->id == 'facility-group')],
                            ['label' => 'Districts', 'url' => ['/records/district'], 'active'=>(Yii::$app->controller->id == 'district')],
                            ['label' => 'Regions', 'url' => ['/records/region'], 'active'=>(Yii::$app->controller->id == 'region')],
                        ]
                    ])?>
                </div>

            </div>
            <div class="col-md-10">
                <?php $this->params['breadcrumbs'] = ArrayHelper::merge([
                    ['label'=>'Records', 'url'=>['/records']]
                ], $this->params['breadcrumbs'])?>
                <?=$content?>
            </div>
        </div>
    </div>
<?php $this->endContent();?>
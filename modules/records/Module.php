<?php

namespace app\modules\records;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\records\controllers';
    public $layout = 'records';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        // your custom code here
        if(\Yii::$app->user->isGuest)
            \Yii::$app->user->loginRequired();

        return true; // or false to not run the action
    }
}
